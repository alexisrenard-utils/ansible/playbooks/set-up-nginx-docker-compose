#  Dockerized nginx application (with certbot certification)

Setting up a webserver with a dockerized nginx. 
## Usage

* Clone the project and its submodule(s)
```bash
git clone --recurse-submodules <this repo>
```
* Initiate the submodule(s) recursively - the right way
```bash
git submodule update --init --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```
* You're ready to go !

☝️ *git version 2.17.1*

## Variables:

### Role : `certbot-certif-generation`
* **`certbot_email`**

  * Default : `renard.alexis@hotmail.fr`
* **`domain_name`**

  * Default : `renardalexis.com`
* **`domain_name_www`**

  * Default : `www.renardalexis.com`
### Role : `nginx-docker-compose`
* **`letsencrypt_remote_dir`**

  * Default : `/etc/letsencrypt`
* **`remote_install_directory_path`**

  * Default : `"/home/{{ docker_user }}/{{ playbook_name }}"`
* **`create_default_website`**

  * Default : `false`
* **`use_new_docker_environment`**

  * Default : `false`
* **`docker_user`**

  * The user that will perform the docker commands.
  * Default : `alexis`
* **`set_up_docker_group_and_user`**

  * Create new docker group and user.
  * Default : `true`
* **`create_default_containers`**

  * Create default container (mainly used to test that it's working fine.)
  * Default : `false`
* **`default_container_name`**

  * The default test container name.
  * Default : `docker-ubuntu`
* **`default_container_image`**

  * The default test docker image.
  * Default : `ubuntu`
* **`default_container_command`**

  * The default test container command to be executed.
  * Default : `sleep 1d`
## Tags:

* `certbot`


* `apt`


* `packages`


* `apt_key`


* `apt_repository`


* `pip`


* `docker`


* `system`


* `nginx`

## License
Project under the
 [MIT](https://tldrlegal.com/license/mit-license)
 License.

## Appendix
### Working with git submodules

* Add a submodule
```bash
git submodule add <repo_url>
```

* Update the submodules
```bash
git submodule update --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```

* Remove a gitsubmodule
```bash
rm -rf <path_to_the_module>
# -> Then stage the potential modifications you've done in the .gitmodule file.
git rm -r --cached <path_to_the_module>
# -> Then open and remove submodule entry in .gitmodules file.
```
*git version 2.17.1*



## Author Information
This playbook  was created by: [alexis_renard](https://renardalexis.com)

👉 https://fr.linkedin.com/in/renardalexis


## Automatically generated
Documentation generated using ansible-autodoc : [https://gitlab.com/alexisrenard-utils/ansible/ansible-autodoc](https://gitlab.com/alexisrenard-utils/ansible/ansible-autodoc)